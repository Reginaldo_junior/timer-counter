
//-------------------- Tipos de dados --------------------  
#define __IO volatile  //Habilita RW
#define uint32_t unsigned int
#define u32 unsigned int
#define int32_t int

#define uint16_t unsigned short
#define u16 unsigned short

#define STACKINT	0x20000000

//--------------------  GPIOx Def --------------------
#define GPIOA ((GPIO_TypeDef * ) 0x40010800) /* GPIOx Addr Base */
#define GPIOB ((GPIO_TypeDef * ) 0x40010c00) 
#define GPIOC ((GPIO_TypeDef * ) 0x40011000) 
#define GPIOD ((GPIO_TypeDef * ) 0x40011400) 
#define GPIOE ((GPIO_TypeDef * ) 0x40011800) 
#define GPIOF ((GPIO_TypeDef * ) 0x40011c00) 
#define GPIOG ((GPIO_TypeDef * ) 0x40012000) 

//-------------------- TIMx Def -------------------- 
#define TIM1 ((TIM_TypeDef * ) 0x40012c00) /* TIMx Addr Base */
#define TIM2 ((TIM_TypeDef * ) 0x40000000) 
#define TIM3 ((TIM_TypeDef * ) 0x40000400) 
#define TIM4 ((TIM_TypeDef * ) 0x40000800) 
#define TIM5 ((TIM_TypeDef * ) 0x40000c00) 
#define TIM6 ((TIM_TypeDef * ) 0x40001000) 
#define TIM7 ((TIM_TypeDef * ) 0x40001400) 
#define TIM8 ((TIM_TypeDef * ) 0x40013400) 
#define TIM9 ((TIM_TypeDef * ) 0x40014C00) 
#define TIM10 ((TIM_TypeDef * ) 0x40015000) 
#define TIM11 ((TIM_TypeDef * ) 0x40015400) 
#define TIM12 ((TIM_TypeDef * ) 0x40001800) 
#define TIM13 ((TIM_TypeDef * ) 0x40001c00) 
#define TIM14 ((TIM_TypeDef * ) 0x40002000) 
#define FLASH (( FLASH_TypeDef * ) 0x40022000)

//-------------------- AFIO 
#define AFIO (( AFIO_TypeDef *) 0x40010000 )

//-------------------- NVIC 
#define NVIC (( NVIC_type * ) 0xE000E000) // ou 0xE000E100

//-------------------- RCC Def -------------------- 
#define RCC (( RCC_TypeDef *) 0x40021000) /* RCC Addr Base */

//-------------------- Area de funções ---------------  

typedef struct{
	__IO u32 CRL ; 		// 0x00 MODO de operação Port configuration register low
	__IO u32 CRH ; 		// 0x04 Port configuration register high
	__IO u32 IDR ;		// 0x08 Port input data register
	__IO u32 ODR ;		// 0x0c Port output data register
	__IO u32 BSRR ;		// 0x10 Port bit set/reset register
	__IO u32 BRR ;		// 0x14 Port bit reset register
	__IO u32 LCKR ;		// 0x18 Port configuration lock register

} GPIO_TypeDef ;

typedef struct{
		
	__IO u32 EVCR;		// 0x00
	__IO u32 MAPR;		// 0x04
	__IO u32 EXTICR1; 	// 0X08
	__IO u32 EXTICR2; 	// 0x0c
	__IO u32 EXTICR3; 	// 0x10
	__IO u32 EXTICR4; 	// 0x14
	__IO u32 reservado;	// 0x18
	__IO u32 MAPR2;		// 0x1c

} AFIO_TypeDef ;

typedef struct{

	__IO u32 CR1; 		// 0x00
	__IO u32 CR2;		// 0x04
	__IO u32 SMCR;		// 0x08
	__IO u32 DIER;		// 0x0c
	__IO u32 SR; 		// 0x10
	__IO u32 EGR; 		// 0x14
	__IO u32 CCMR1;		// 0x18 ->  input/output capture mode
	__IO u32 CCMR2;		// 0x1c ->  input/output capture mode
	__IO u32 CCER; 		// 0x20 <--- 
	__IO u32 CNT; 		// 0x24
	__IO u32 PSC;		// 0x28
	__IO u32 ARR;		// 0x2c
	__IO u32 RCR;		// 0x30
	__IO u32 CCR1; 		// 0x34
	__IO u32 CCR2;		// 0x38 
	__IO u32 CCR3;		// 0x3c
	__IO u32 CCR4;		// 0x40
	__IO u32 BDTR;		// 0x44 -> Descritivo na pagina 344 do RM0008
	__IO u32 DCR;		// 0x48
	__IO u32 DMAR;		// 0x4c

} TIM_TypeDef ;

typedef struct{
//		       		  Offset
	__IO u32 CR; 		// 0x00
	__IO u32 CFGR;		// 0x04 
	__IO u32 CIR;		// 0x08
	__IO u32 APB2RSTR;	// 0x0c
	__IO u32 APB1RSTR;	// 0x10
	__IO u32 AHBENR;	// 0x14
	__IO u32 APB2ENR;	// 0x18
	__IO u32 APB1ENR;	// 0x1c
	__IO u32 BDCR;		// 0x20
	__IO u32 CSR;		// 0x24
	__IO u32 AHBRSTR;	// 0x28
	__IO u32 CFGR2;		// 0x2c

} RCC_TypeDef;


typedef struct
{
	__IO u32   ISER[8];     /* Address offset: 0x000 - 0x01C */
	__IO u32  RES0[24];     /* Address offset: 0x020 - 0x07C */
	__IO u32   ICER[8];     /* Address offset: 0x080 - 0x09C */
	__IO u32  RES1[24];     /* Address offset: 0x0A0 - 0x0FC */
	__IO u32   ISPR[8];     /* Address offset: 0x100 - 0x11C */
	__IO u32  RES2[24];     /* Address offset: 0x120 - 0x17C */
	__IO u32   ICPR[8];     /* Address offset: 0x180 - 0x19C */
	__IO u32  RES3[24];     /* Address offset: 0x1A0 - 0x1FC */
	__IO u32   IABR[8];     /* Address offset: 0x200 - 0x21C */
	__IO u32  RES4[56];     /* Address offset: 0x220 - 0x2FC */
	__IO u32   IPR[240];     /* Address offset: 0x300 - 0x3EC */
	__IO u32 RES5[644];     /* Address offset: 0x3F0 - 0xEFC */
	__IO u32       STIR;    /* Address offset:         0xF00 */

} NVIC_type;

/*
 * STM32F107 Interrupt Number Definition
 */
typedef enum IRQn
{
	NonMaskableInt_IRQn         = -14,    /* 2 Non Maskable Interrupt                             */
	MemoryManagement_IRQn       = -12,    /* 4 Cortex-M3 Memory Management Interrupt              */
	BusFault_IRQn               = -11,    /* 5 Cortex-M3 Bus Fault Interrupt                      */
	UsageFault_IRQn             = -10,    /* 6 Cortex-M3 Usage Fault Interrupt                    */
	SVCall_IRQn                 = -5,     /* 11 Cortex-M3 SV Call Interrupt                       */
	DebugMonitor_IRQn           = -4,     /* 12 Cortex-M3 Debug Monitor Interrupt                 */
	PendSV_IRQn                 = -2,     /* 14 Cortex-M3 Pend SV Interrupt                       */
	SysTick_IRQn                = -1,     /* 15 Cortex-M3 System Tick Interrupt                   */
	WWDG_IRQn                   = 0,      /* Window WatchDog Interrupt                            */
	PVD_IRQn                    = 1,      /* PVD through EXTI Line detection Interrupt            */
	TAMPER_IRQn                 = 2,      /* Tamper Interrupt                                     */
	RTC_IRQn                    = 3,      /* RTC global Interrupt                                 */
	FLASH_IRQn                  = 4,      /* FLASH global Interrupt                               */
	RCC_IRQn                    = 5,      /* RCC global Interrupt                                 */
	EXTI0_IRQn                  = 6,      /* EXTI Line0 Interrupt                                 */
	EXTI1_IRQn                  = 7,      /* EXTI Line1 Interrupt                                 */
	EXTI2_IRQn                  = 8,      /* EXTI Line2 Interrupt                                 */
	EXTI3_IRQn                  = 9,      /* EXTI Line3 Interrupt                                 */
	EXTI4_IRQn                  = 10,     /* EXTI Line4 Interrupt                                 */
	DMA1_Channel1_IRQn          = 11,     /* DMA1 Channel 1 global Interrupt                      */
	DMA1_Channel2_IRQn          = 12,     /* DMA1 Channel 2 global Interrupt                      */
	DMA1_Channel3_IRQn          = 13,     /* DMA1 Channel 3 global Interrupt                      */
	DMA1_Channel4_IRQn          = 14,     /* DMA1 Channel 4 global Interrupt                      */
	DMA1_Channel5_IRQn          = 15,     /* DMA1 Channel 5 global Interrupt                      */
	DMA1_Channel6_IRQn          = 16,     /* DMA1 Channel 6 global Interrupt                      */
	DMA1_Channel7_IRQn          = 17,     /* DMA1 Channel 7 global Interrupt                      */
	ADC1_2_IRQn                 = 18,     /* ADC1 and ADC2 global Interrupt                       */
	CAN1_TX_IRQn                = 19,     /* USB Device High Priority or CAN1 TX Interrupts       */
	CAN1_RX0_IRQn               = 20,     /* USB Device Low Priority or CAN1 RX0 Interrupts       */
	CAN1_RX1_IRQn               = 21,     /* CAN1 RX1 Interrupt                                   */
	CAN1_SCE_IRQn               = 22,     /* CAN1 SCE Interrupt                                   */
	EXTI9_5_IRQn                = 23,     /* External Line[9:5] Interrupts                        */
	TIM1_BRK_IRQn               = 24,     /* TIM1 Break Interrupt                                 */
	TIM1_UP_IRQn                = 25,     /* TIM1 Update Interrupt                                */
	TIM1_TRG_COM_IRQn           = 26,     /* TIM1 Trigger and Commutation Interrupt               */
	TIM1_CC_IRQn                = 27,     /* TIM1 Capture Compare Interrupt                       */
	TIM2_IRQn                   = 28,     /* TIM2 global Interrupt                                */
	TIM3_IRQn                   = 29,     /* TIM3 global Interrupt                                */
	TIM4_IRQn                   = 30,     /* TIM4 global Interrupt                                */
	I2C1_EV_IRQn                = 31,     /* I2C1 Event Interrupt                                 */
	I2C1_ER_IRQn                = 32,     /* I2C1 Error Interrupt                                 */
	I2C2_EV_IRQn                = 33,     /* I2C2 Event Interrupt                                 */
	I2C2_ER_IRQn                = 34,     /* I2C2 Error Interrupt                                 */
	SPI1_IRQn                   = 35,     /* SPI1 global Interrupt                                */
	SPI2_IRQn                   = 36,     /* SPI2 global Interrupt                                */
	USART1_IRQn                 = 37,     /* USART1 global Interrupt                              */
	USART2_IRQn                 = 38,     /* USART2 global Interrupt                              */
	USART3_IRQn                 = 39,     /* USART3 global Interrupt                              */
	EXTI15_10_IRQn              = 40,     /* External Line[15:10] Interrupts                      */
	RTCAlarm_IRQn               = 41,     /* RTC Alarm through EXTI Line Interrupt                */
	OTG_FS_WKUP_IRQn            = 42,     /* USB OTG FS WakeUp from suspend through EXTI Line Int */
	TIM5_IRQn                   = 50,     /* TIM5 global Interrupt                                */
	SPI3_IRQn                   = 51,     /* SPI3 global Interrupt                                */
	UART4_IRQn                  = 52,     /* UART4 global Interrupt                               */
	UART5_IRQn                  = 53,     /* UART5 global Interrupt                               */
	TIM6_IRQn                   = 54,     /* TIM6 global Interrupt                                */
	TIM7_IRQn                   = 55,     /* TIM7 global Interrupt                                */
	DMA2_Channel1_IRQn          = 56,     /* DMA2 Channel 1 global Interrupt                      */
	DMA2_Channel2_IRQn          = 57,     /* DMA2 Channel 2 global Interrupt                      */
	DMA2_Channel3_IRQn          = 58,     /* DMA2 Channel 3 global Interrupt                      */
	DMA2_Channel4_IRQn          = 59,     /* DMA2 Channel 4 global Interrupt                      */
	DMA2_Channel5_IRQn          = 60,     /* DMA2 Channel 5 global Interrupt                      */
	ETH_IRQn                    = 61,     /* Ethernet global Interrupt                            */
	ETH_WKUP_IRQn               = 62,     /* Ethernet Wakeup through EXTI line Interrupt          */
	CAN2_TX_IRQn                = 63,     /* CAN2 TX Interrupt                                    */
	CAN2_RX0_IRQn               = 64,     /* CAN2 RX0 Interrupt                                   */
	CAN2_RX1_IRQn               = 65,     /* CAN2 RX1 Interrupt                                   */
	CAN2_SCE_IRQn               = 66,     /* CAN2 SCE Interrupt                                   */
	OTG_FS_IRQn                 = 67      /* USB OTG FS global Interrupt                          */

} IRQn_type;


void enable_interrupt(IRQn_type IRQn)
{
	NVIC->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));
}


void SystemInit (void) {

  /* Reset the RCC clock configuration to the default reset state(for debug purpose) */
  /* Set HSION bit */
  RCC->CR |= (uint32_t)0x00000001;

  /* Reset SW, HPRE, PPRE1, PPRE2, ADCPRE and MCO bits */
  RCC->CFGR &= (uint32_t)0xF8FF0000;
 
  /* Reset HSEON, CSSON and PLLON bits */
  RCC->CR &= (uint32_t)0xFEF6FFFF;

  /* Reset HSEBYP bit */
  RCC->CR &= (uint32_t)0xFFFBFFFF;

  /* Reset PLLSRC, PLLXTPRE, PLLMUL and USBPRE/OTGFSPRE bits */
  RCC->CFGR &= (uint32_t)0xFF80FFFF;

  /* Disable all interrupts and clear pending bits  */
  RCC->CIR = 0x009F0000;
 }



void set_system_clock_to_25Mhz (void){
	RCC->CR |= 1<<16 ;
	while (!( RCC->CR & (1<<17) ));	
	RCC->CFGR |= 1<<0;
}

int32_t main (void) {

/*-------------------- TIMER/COUNTER 2 ---------------*/
	RCC->APB2ENR |= ( 1<<2 ) ; //Habilita a GPIOA

	set_system_clock_to_25Mhz();

	GPIOA->CRL &= ~( 0xffffffff );
	GPIOA->CRH &= ~( 0xffffffff );

	GPIOA->CRL |= (0x0B << 4) ;
	GPIOA->CRH |= (0x00B << 4) ; /* Configurado 1011 para o pino A1 = CH2;
       				          AF; output push-pull max 50 MHz*/ 


	RCC->APB2ENR |= 1 << 0 ; /*Habilita o AFIO */
	RCC->APB1ENR |= 1 << 0; /*Habilita o TIMER 2 - CH na GPIO_A 
				pin: 10 - 13 (ref = diagrama blue pill. */

		/* | 3| 2|  | 1| 0|  8 ; 0  */
	

	AFIO->MAPR &= ~( 0b111 << 24 ); // reseta os bits do SWJ_CFG  // possível causador de problemas
	AFIO->MAPR |= ( 0 << 8); 
					/* [bit 26:24] SWJ_CFG [nota_2]
					     [bit 8] TIM3_REMAP[1:0]: TIM2 remapping [nota_3]

					     Configura a GPIOA como Alternative Func. Modo output */


	TIM2->CR1 &= ~( 1<<0 | 0b11<<8 | 
			0xf << 4  ); /*	[bit 0]Desabilitado o clock 
				       	[bit 4]Define a DIReção, 0=up-counting or 1=Down-counting.  
					[bit 5:6] clock division sem constante de mult.
					[bit 7] Auto-reload preload 0= not buffered 
					[bit 8:9] Define fator escalar de divisão  
					*/

	TIM2->PSC = 3999 ; /*Def, o valor do clock "prescalar" */

	TIM2->ARR = ( 2000 - 1); /* Valor do Auto-reload register, 
				   // define o overflow (estouro) do contador de clock.
				   // Se for up-counting ( 0..>ARR ) 
				   // ou down-counting ( ARR..>0  )  */

	
	TIM2->BDTR |= 1<< 15; /*Habilita a MOE (MAIN OUTPUT ENABLE) */
	
	/* TIM2->CCER &= ~( 1<<4 ); //Desabilita o OC2 (output Compare 2) SEM ---MOE--- */

	TIM2->CCER |= ( 1<<4 | 1<<6); //Habilita CC2E, CC2NE  (cap/Compare 2)_Pag 339 (Advanced-control Timers)
	TIM2->CCMR1 |= ( 0b011 << 12 ) ; /* [bit 14:12] OCM_Ch2; [bit 6:4] OCM_Ch1 
				          * 110 : PWM Mode -> (up-cont.) 
					  * 	Se mantém ativo enquanto CNT<CCR2 (Cap/Comp Register 2)
					  * 011: Output compare mode
					  */
	//TIM2->BDTR &=~( 0b11 << 8); /*[bit 9:8] - LOCK:
       	//			     * 00 - Destrava o bit de protecao para definir o npivel de trabalho 
	//			     * da porta referente ao registrador CCER */

	TIM2->CCER &= ~( 1<<7  ); /*  CC2NP ativo em nível (0 = High; 1 = Low) 
				   *  configuração do Cap/Comp do canal 2  
				   * */
	
	TIM2->CR1 |= 1<<0; //Habilita o clock
	TIM2->CCR1 = 500; //  [nota_1] Não é necessário neste estágio

	while(1);
	

	return 0;

 //o // de proposito
}

/* Definições de bits "CFG" e "Mode" da configuração das GPIOs.
 *   CFG     MODE    H	 L
 * |31|30|  |29|28| 15 ; 7
 * |27|26|  |25|24| 14 ; 6	
 * |23|22|  |21|20| 13 ; 5
 * |19|18|  |17|16| 12 ; 4
 * |15|14|  |13|12| 11 ; 3
 * |11|10|  | 9| 8| 10 ; 2
 * | 7| 6|  | 5| 4|  9 ; 1
 * | 3| 2|  | 1| 0|  8 ; 0
 *
 *
 * __________________________________________
 * | 0| 0|  |01..11| <- Out Push-pull
 * | 0| 1|  |01..11| <- Out Open-drain
 * | 1| 0|  |01..11| <- Alt Out *pull
 * | 1| 0|  |01..11| <- Alt Out *drain
 * __________________________________________
 * | 0| 0|  |  00  | -< Input Analogig
 * | 0| 1|  |  00  | -< Input Floating  
 * | 1| 0|  |  00  | -< Input Pull-{down,up}
 * __________________________________________
 * */


/*
 * nota_1 -----------------------------------------------------
	 * Output mode: Flag is set by hardware when the counter matches the compare value, with some exception in center-aligned mode (refer to the CMS bits in the TIMx_CR1 register description). It is cleared by software.
	0: No match. 
	1: The content of the counter TIMx_CNT has matched the content of the TIMx_CCR1 register.
*
*
* nota_2 -----------------------------------------------------
* 	Bits 26:24 SWJ_CFG[2:0]: Serial wire JTAG configuration These bits are write-only (when read, the value is undefined). They are used to configure the SWJ and trace alternate function I/Os. The SWJ (Serial Wire JTAG) supports JTAG or SWD access to the Cortex debug port. The default state after reset is SWJ ON without trace. This allows JTAG or SW mode to be enabled by sending a specific sequence on the JTMS / JTCK pin. 
* 	000: Full SWJ (JTAG-DP + SW-DP): Reset State 
* 	001: Full SWJ (JTAG-DP + SW-DP) but without NJTRST 
* 	010: JTAG-DP Disabled and SW-DP Enabled 
* 	100: JTAG-DP Disabled and SW-DP Disabled Other combinations: no effect
*
*
* nota_3 -----------------------------------------------------
* 	Bits 9:8 TIM2_REMAP[1:0]: TIM2 remapping These bits are set and cleared by software. They control the mapping of TIM2 channels 1 to 4 and external trigger (ETR) on the GPIO ports. 
* 	00: No remap (CH1/ETR/PA0, CH2/PA1, CH3/PA2, CH4/PA3) 
* 	01: Partial remap (CH1/ETR/PA15, CH2/PB3, CH3/PA2, CH4/PA3) 
* 	10: Partial remap (CH1/ETR/PA0, CH2/PA1, CH3/PB10, CH4/PB11) 
* 	11: Full remap (CH1/ETR/PA15, CH2/PB3, CH3/PB10, CH4/PB11)
*/

